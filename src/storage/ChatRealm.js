import Realm from "realm"

const ChatSchema = {
    name: 'Chat',
    properties: {
        messageId: 'int',
        message: 'string',
        mediaUrl: 'string',
        mediaId: 'int',
        time: 'string',
        messageKind: 'int',
        receiverId: 'int',
        talkId: 'int',
    },
    primaryKey: 'messageId'
}

export const writeChats = async (chats) => {
    const realm = await Realm.open({
        path: 'ChatsList',
        schema: [ChatSchema]
    })

    realm.write(() => {
        realm.delete(realm.objects('Chat'))
        chats?.map(item => {
            realm.create('Chat', {
                messageId: item.messageId,
                message: item.message,
                mediaUrl: (item.mediaUrl == null) ? '' : item.mediaUrl,
                mediaId: item.mediaId,
                time: item.time,
                messageKind: item.messageKind,
                receiverId: item.userId,
                talkId: item.talkId
            })
        })
    })
    realm.close()
}

export const writeMoreChats = async (moreChats) => {
    const realm = await Realm.open({
        path: 'ChatsList',
        schema: [ChatSchema]
    })

    realm.write(() => {
        moreChats?.map(item => {
            realm.create('Chat', {
                messageId: item.messageId,
                message: item.message,
                mediaUrl: (item.mediaUrl == null) ? '' : item.mediaUrl,
                mediaId: item.mediaId,
                time: item.time,
                messageKind: item.messageKind,
                receiverId: item.userId,
                talkId: item.talkId
            })
        })
    })
    realm.close()
}

export const readChats = async (talkId) => {
    const realm = await Realm.open({
        path: 'ChatsList',
        schema: [ChatSchema]
    })

    const msg = realm.objects('Chat')
    // if (msg.find(item => item.receiverId == talkId) || msg.length == 1) {
    //     console.log('realm chat ini : ', JSON.stringify(msg, null, 2))
    //     return msg
    // }

    if (talkId == msg[0]?.talkId && msg.length > 0) {
        console.log('realm chat ini : ', JSON.stringify(msg, null, 2))
        return msg
    }
    return []
}

export const deleteAllChats = async () => {
    const realm = await Realm.open({
        path: 'ChatsList',
        schema: [ChatSchema]
    })

    realm.write(() => {
        realm.delete(realm.objects('Chat'))
    })
    realm.close()
}