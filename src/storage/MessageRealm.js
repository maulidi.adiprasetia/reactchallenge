import Realm from "realm"

const MessageSchema = {
    name: 'Message',
    properties: {
        talkId: 'int',
        imageUrl: 'string',
        nickname: 'string',
        message: 'string',
    },
    primaryKey: 'talkId'
}

export const writeMessages = async (messages) => {
    const realm = await Realm.open({
        path: 'MessagesList',
        schema: [MessageSchema]
    })

    realm.write(() => {
        realm.delete(realm.objects('Message'))
        messages.map(item => {
            realm.create('Message', {
                talkId: item.talkId,
                imageUrl: (item.imageUrl == null) ? '' : item.imageUrl,
                nickname: item.nickname,
                message: item.message,
            })
        })
    })
    realm.close()
}

export const readMessages = async () => {
    const realm = await Realm.open({
        path: 'MessagesList',
        schema: [MessageSchema]
    })

    const msg = realm.objects('Message')
    console.log('realm ini : ', JSON.stringify(msg, null, 2))
    return msg
}

export const removeMessages = async (messages) => {
    const realm = await Realm.open({
        path: 'MessagesList',
        schema: [MessageSchema]
    })

    realm.write(() => {
        messages.map(item => {
            const msg = realm.objects('Message').filtered("talkId = " + item.talkId.toString())
            realm.delete(msg)
        })
    })
    realm.close()
}

export const deleteAllMessages = async () => {
    const realm = await Realm.open({
        path: 'MessagesList',
        schema: [MessageSchema]
    })

    realm.write(() => {
        realm.delete(realm.objects('Message'))
    })

    realm.close()
}