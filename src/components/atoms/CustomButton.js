import React from 'react';
import { StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import Colors from '../../constants/Colors';

const CustomButton = props => {
    return <Button
        {...props}
        styles={{ ...props.styles }}
        disabledTitleStyle={{ ...styles.disbledTitle }}
        disabledStyle={{ ...styles.buttonDisabled }}
        buttonStyle={
            props.types == "primary" ?
                styles.primary :
                props.types == 'secondary' ?
                    styles.secondary :
                    props.types == 'dangerPrimary' ?
                        styles.danger :
                        styles.dangerSecondary
        }
        titleStyle={
            props.types == "primary" ?
                styles.titlePrimary :
                props.types == 'secondary' ?
                    styles.titleSecondary :
                    props.types == 'dangerPrimary' ?
                        styles.titleDanger :
                        styles.titleDangerSecondary
        }
    />
};

const styles = StyleSheet.create({
    primary: {
        borderRadius: 12,
        borderWidth: 1,
        backgroundColor: Colors.accent,
    },
    danger: {
        borderRadius: 12,
        borderWidth: 1,
        backgroundColor: Colors.danger,
    },
    secondary: {
        borderRadius: 12,
        borderWidth: 2,
        borderColor: Colors.accent,
        backgroundColor: 'white',
    },
    dangerSecondary: {
        borderRadius: 12,
        borderWidth: 2,
        borderColor: Colors.danger,
        backgroundColor: 'white',
    },
    buttonDisabled: {
        borderRadius: 12,
        borderWidth: 1,
        backgroundColor: Colors.greyLight,
        borderColor: Colors.greyLight,
    },
    titlePrimary: {
        fontSize: 18,
        fontFamily: 'Rubik-Bold'
    },
    titleDanger: {
        fontSize: 18,
        fontFamily: 'Rubik-Bold'
    },
    titleSecondary: {
        fontSize: 18,
        fontFamily: 'Rubik-Bold',
        color: '#1644BD'
    },
    titleDangerSecondary: {
        fontSize: 18,
        fontFamily: 'Rubik-Bold',
        color: Colors.danger
    },
    disbledTitle: {
        fontFamily: 'roboto-bold',
        color: 'white'
    }
});

export default CustomButton
