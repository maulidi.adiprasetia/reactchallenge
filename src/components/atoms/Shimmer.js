import React from "react";
import { Image, StyleSheet, View } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { createShimmerPlaceholder } from "react-native-shimmer-placeholder";
import styles from "../../constants/Styles";

const shimmerStyle = StyleSheet.create({
    feedImage: {
        width: 150,
        height: 150,
        borderRadius: 75,
        marginBottom: 16,
        alignSelf: 'center'
    },
    feedName: {
        maxWidth: 150,
        alignSelf: 'center'
    },
    feedContainer: {
        width: '50%',
        marginBottom: 20
    },
    detailImage: {
        width: '100%',
        height: 250,
    },
    detailContent: {
        alignSelf: 'center'
    },
    content: {
        width: '100%',
        marginBottom: 16
    },
    messageContainer: {
        margin: 16,
        paddingBottom: 16,
        flexDirection: 'row',
        alignItems: 'center',
    },
    messageImg: {
        width: 80,
        height: 80,
        borderRadius: 40,
        marginRight: 16
    }
})

const ShimmerPlaceholder = createShimmerPlaceholder(LinearGradient)

export const feedShimmer = () => {
    return (
        <View style={[styles.contentWrapper, { flexWrap: 'wrap' }, { flexDirection: 'row' }]}>
            <View style={shimmerStyle.feedContainer}>
                <ShimmerPlaceholder style={shimmerStyle.feedImage} />
                <ShimmerPlaceholder style={shimmerStyle.feedName} />
            </View>
            <View style={shimmerStyle.feedContainer}>
                <ShimmerPlaceholder style={shimmerStyle.feedImage} />
                <ShimmerPlaceholder style={shimmerStyle.feedName} />
            </View>
            <View style={shimmerStyle.feedContainer}>
                <ShimmerPlaceholder style={shimmerStyle.feedImage} />
                <ShimmerPlaceholder style={shimmerStyle.feedName} />
            </View>
            <View style={shimmerStyle.feedContainer}>
                <ShimmerPlaceholder style={shimmerStyle.feedImage} />
                <ShimmerPlaceholder style={shimmerStyle.feedName} />
            </View>
            <View style={shimmerStyle.feedContainer}>
                <ShimmerPlaceholder style={shimmerStyle.feedImage} />
                <ShimmerPlaceholder style={shimmerStyle.feedName} />
            </View>
            <View style={shimmerStyle.feedContainer}>
                <ShimmerPlaceholder style={shimmerStyle.feedImage} />
                <ShimmerPlaceholder style={shimmerStyle.feedName} />
            </View>
        </View>
    )
}

export const feedShimmerEndList = () => {
    return (
        <View style={[styles.contentWrapper, { flexDirection: 'row' }, { justifyContent: 'space-between' }]}>
            <View>
                <ShimmerPlaceholder style={shimmerStyle.feedImage} />
                <ShimmerPlaceholder style={shimmerStyle.feedName} />
            </View>
            <View>
                <ShimmerPlaceholder style={shimmerStyle.feedImage} />
                <ShimmerPlaceholder style={shimmerStyle.feedName} />
            </View>
        </View>
    )
}

export const userDetailShimmer = () => {
    return (
        <View>
            <ShimmerPlaceholder style={shimmerStyle.detailImage} />
            <View style={styles.contentWrapper}>
                <ShimmerPlaceholder style={shimmerStyle.detailContent} />
                <ShimmerPlaceholder style={[shimmerStyle.detailContent, { width: '70%' }]} />
                <ShimmerPlaceholder style={[shimmerStyle.detailContent, { width: '60%' }, { marginBottom: 16 }]} />
                <ShimmerPlaceholder style={[shimmerStyle.content]} />
                <ShimmerPlaceholder style={[shimmerStyle.content]} />
                <ShimmerPlaceholder style={[shimmerStyle.content]} />
                <ShimmerPlaceholder style={[shimmerStyle.content]} />
            </View>
        </View>
    )
}

export const messageShimmer = () => {
    return (
        <>
            <View style={[shimmerStyle.messageContainer]}>
                <ShimmerPlaceholder style={shimmerStyle.messageImg} />
                <View>
                    <ShimmerPlaceholder />
                    <ShimmerPlaceholder />
                </View>
            </View>
            <View style={[shimmerStyle.messageContainer]}>
                <ShimmerPlaceholder style={shimmerStyle.messageImg} />
                <View>
                    <ShimmerPlaceholder />
                    <ShimmerPlaceholder />
                </View>
            </View>
            <View style={[shimmerStyle.messageContainer]}>
                <ShimmerPlaceholder style={shimmerStyle.messageImg} />
                <View>
                    <ShimmerPlaceholder />
                    <ShimmerPlaceholder />
                </View>
            </View>
            <View style={[shimmerStyle.messageContainer]}>
                <ShimmerPlaceholder style={shimmerStyle.messageImg} />
                <View>
                    <ShimmerPlaceholder />
                    <ShimmerPlaceholder />
                </View>
            </View>
            <View style={[shimmerStyle.messageContainer]}>
                <ShimmerPlaceholder style={shimmerStyle.messageImg} />
                <View>
                    <ShimmerPlaceholder />
                    <ShimmerPlaceholder />
                </View>
            </View>
        </>
    )
}