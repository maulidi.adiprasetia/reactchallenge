import React from 'react';
import { Image, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View } from 'react-native';
import Colors from '../../constants/Colors';

const style = StyleSheet.create({
    navbar: {
        padding: 16,
        alignItems: 'center',
        flexDirection: 'row',
        elevation: 2,
    },

    itemWrapper: {
        flex: 1,
    },

    backBtn: {
        width: 30,
        height: 30,
    },

    navbarTitle: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    profileImg: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    roomToolbar: {
        flexDirection: 'row',
        alignItems: 'center'
    }
})

const CustomToolbar = props => {
    return (
        <View style={[style.navbar, { backgroundColor: props.types != 'dark' && 'white' }]}>
            {
                props.hasBack ?
                    <View style={[style.itemWrapper,]}>
                        <TouchableHighlight
                            style={[{ borderRadius: 20 }, { alignSelf: 'flex-start' }, { padding: 2 }]}
                            underlayColor={Colors.greyLight}
                            activeOpacity={1}
                            onPress={props.onPressBack}>
                            <Image style={style.backBtn} source={
                                props.types == 'dark' ?
                                    require('../../assets/images/back_arrow_white.png') :
                                    require('../../assets/images/back_arrow.png')} />
                        </TouchableHighlight>
                    </View> :
                    <View style={[style.itemWrapper,]} />
            }
            <View style={[{ flex: 3 }]}>
                {
                    props.types == 'chatRoom' ?
                        <>
                            <View style={style.roomToolbar}>
                                <TouchableOpacity
                                    onPress={props.onPressImage}
                                >
                                    <Image style={style.profileImg} source={
                                        props.imageUrl ?
                                            { uri: props.imageUrl } :
                                            require('../../assets/images/profile.png')
                                    } />
                                </TouchableOpacity>
                                <Text style={[style.navbarTitle, { marginLeft: 12 }]}>{props.title}</Text>
                            </View>
                        </> :
                        <Text style={[style.navbarTitle, { textAlign: 'center' }]}>{props.title}</Text>
                }
            </View>
            {
                props.hasMenuBtn ?
                    <View style={[style.itemWrapper,]}>
                        <TouchableHighlight
                            {...props}
                            style={[{ borderRadius: 10 }, { alignSelf: 'flex-end' }, { padding: 2 }]}
                            underlayColor={Colors.greyLight}
                            activeOpacity={1}
                            onPress={props.onPressMenu}>
                            {props.menuBtn}
                        </TouchableHighlight>
                    </View> :
                    <View style={[style.itemWrapper,]} />
            }
        </View>
    )
}

export default CustomToolbar