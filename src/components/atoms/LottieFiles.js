import React from "react";
import AnimatedLottieView from "lottie-react-native";
import { StyleSheet } from "react-native";

const lottieStyles = StyleSheet.create({
    container: {
        height: '100%',
        alignSelf:'center',
    }
})

export const emptyBox = () => {
    return (
        <AnimatedLottieView style={lottieStyles.container} source={require('../../assets/lotties/empty_box.json')} loop autoPlay />
    )
}