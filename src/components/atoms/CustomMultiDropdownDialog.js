import React, { useState } from "react";
import { FlatList, StyleSheet, Text, View } from "react-native";
import DialogManager, { ScaleAnimation, DialogContent } from 'react-native-dialog-component';
import { TouchableHighlight, TouchableOpacity } from "react-native-gesture-handler";
import Colors from '../../constants/Colors';
import { Hobbies } from "../../constants/Constants";
import CustomButton from "./CustomButton";

const dialogStyle = StyleSheet.create({
    container: {
        padding: 12
    },
    dialogTitle: {
        marginBottom: 12,
        fontSize: 20,
        fontWeight: 'bold',
        alignSelf: 'center'
    },
    itemContainer: {
        padding: 8,
        marginBottom: 8,
        borderRadius: 8,
        backgroundColor: Colors.grey
    },
    item: {
        color: 'white'
    }
})

const CustomMultiDropdownDialog = (props) => {
    const data = props.items
    const [selectedHobbies, setSelectedHobbies] = useState([])

    const handleItemClicked = (value) => {
        console.log('clicked : ', value)
    }

    return (
        <View style={dialogStyle.container}>
            <Text style={dialogStyle.dialogTitle}>{props.dialogTitle}</Text>
            <FlatList
                data={Hobbies}
                renderItem={data => (
                    (
                        <TouchableOpacity
                            style={dialogStyle.itemContainer}
                            underlayColor={Colors.greyLight}
                            onPress={() => {
                                console.log(data)
                            }}
                        >
                            <View>
                                <Text style={dialogStyle.item}>{data.item.hobby}</Text>
                            </View>
                        </TouchableOpacity>
                    )
                )}
                keyExtractor={item => item.id.toString()}
                scrollEnabled={true}
            />
            <View style={{ flexDirection: 'row', display: 'flex', marginTop: 10 }}>
                <View style={{ flex: 1 }}>
                    <CustomButton types='secondary' onPress={() => { DialogManager.dismiss() }} title='Cancel' />
                </View>
                <View style={{ flex: 1, marginLeft: 10 }}>
                    <CustomButton types='primary' onPress={() => { }} title='OK' />
                </View>
            </View>
        </View>
    )
}

export default CustomMultiDropdownDialog