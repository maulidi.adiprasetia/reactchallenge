import React from 'react';
import { ActivityIndicator, View } from 'react-native';
import Colors from '../../constants/Colors';
import styles from '../../constants/Styles';

const CustomLoader = () => {
    return (
        <View style={styles.loadingIndicator}>
            <ActivityIndicator size={70} color={Colors.accent} />
        </View>
    )
}

export default CustomLoader