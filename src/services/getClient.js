import axios from "axios";
import * as AxiosLogger from 'axios-logger';


const clientConfig = axios.create({
    baseURL: 'https://terraresta.com/app/api'
})


clientConfig.interceptors.request.use(
    async (config) => {
        config.headers['Content-Type'] = 'application/x-www-form-urlencoded'
        config.headers.Accept = 'application/json'
        AxiosLogger.responseLogger

        return config
    },
    (err) => {
        return Promise.reject(err)
    }
)

clientConfig.interceptors.response.use(AxiosLogger.responseLogger)

export default clientConfig