import axios from 'axios'
import { useCancelToken } from './cancelRequest'
import getClient from './getClient'
import { getUserCreds } from './profileServices'

export const getMessages = async () => {
    const [accessToken] = await getUserCreds()
    const response = await getClient.get(
        '/TalkCtrl/TalkList', {
        params: {
            access_token: accessToken
        }
    }
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

const getTalkIds = (selectedMsg) => {
    let selectedIds = ''
    if (selectedMsg.length > 1) {
        selectedMsg.forEach((item, index) => {
            selectedIds = (index != selectedMsg.length - 1) ?
                selectedIds + item.talkId + ', ' :
                selectedIds + item.talkId
        });
    } else {
        selectedIds = selectedMsg[0].talkId
    }

    return selectedIds
}

export const deleteMessages = async (selectedMsg) => {
    const selectedIds = getTalkIds(selectedMsg)
    // const cancelTokens = axios.CancelToken.source()

    const [accessToken] = await getUserCreds()
    const response = await getClient.get(
        '/TalkCtrl/Delete', {
        // cancelToken: newCancelToken(),
        params: {
            access_token: accessToken,
            talk_ids: selectedIds.toString()
        }
    }
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

export const getChats = async (toUserId) => {
    console.log(toUserId)

    const [accessToken] = await getUserCreds()
    const response = await getClient.get(
        '/TalkCtrl/Talk', {
        params: {
            access_token: accessToken,
            to_user_id: toUserId,
            border_message_id: 0,
            how_to_request: 0,
        }
    }
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

export const getMoreChats = async (toUserId, msgId) => {
    const [accessToken] = await getUserCreds()
    const response = await getClient.get(
        '/TalkCtrl/Talk', {
        params: {
            access_token: accessToken,
            to_user_id: toUserId,
            border_message_id: msgId,
            how_to_request: 1,
        }
    }
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

export const sendChat = async (toUserId, message, imageId) => {
    const [accessToken] = await getUserCreds()
    const formData = new FormData()
    formData.append('message', message)
    const response = await getClient.post(
        '/TalkCtrl/SendMessage',
        formData, {
        params: {
            access_token: accessToken,
            to_user_id: toUserId,
            image_id: imageId ? imageId : null
        }
    }
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

export const uploadImage = async (img) => {
    const [accessToken] = await getUserCreds()
    const file = {
        uri: img['uri'],
        name: img['fileName'],
        type: img['type']
    }
    const formData = new FormData()
    formData.append('data', file)
    const response = await getClient.post(
        '/MediaCtrl/ImageUpload?access_token=' + accessToken + '&location=Talk',
        formData
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

export const sendImage = async (toUserId, imageId) => {
    const [accessToken] = await getUserCreds()
    const response = await getClient.post(
        '/TalkCtrl/SendMessage',
        null, {
        params: {
            access_token: accessToken,
            to_user_id: toUserId,
            image_id: imageId ? imageId : null
        }
    }
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}