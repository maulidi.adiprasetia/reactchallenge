import getClient from './getClient'
import { getUserCreds } from './profileServices'

export const getFeed = async () => {
    const [accessToken] = await getUserCreds()
    const response = await getClient.get(
        '/ProfileFeedCtrl/ProfileFeed?access_token=' + accessToken,
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

export const getMoreFeed = async (lastLoginTime) => {
    const [accessToken] = await getUserCreds()
    const response = await getClient.get(
        '/ProfileFeedCtrl/ProfileFeed?access_token=' + accessToken + '&last_login_time=' + lastLoginTime
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}