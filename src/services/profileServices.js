import AsyncStorage from '@react-native-async-storage/async-storage'
import getClient from './getClient'
import { UserCreds } from '../constants/Constants'


export const getUserCreds = async () => {
    const accessToken = await AsyncStorage.getItem(UserCreds.ACCESS_TOKEN_KEY)
    const userId = await AsyncStorage.getItem(UserCreds.USER_ID_KEY)

    return [accessToken, userId]
}

export const deleteAvatar = async (imageId) => {
    const [accessToken] = await getUserCreds()
    const formData = new FormData()
    formData.append('image_id', imageId)
    const response = await getClient.post(
        '/ProfileCtrl/ProfileEdit?access_token=' + accessToken,
        formData
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

export const getProfile = async () => {
    const [accessToken, userId] = await getUserCreds()
    const response = await getClient.get('/ProfileCtrl/ProfileDisplay', {
        params: {
            access_token: accessToken,
            user_id: userId
        }
    })
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

export const getProfileDetail = async (userId) => {
    const [accessToken] = await getUserCreds()
    const response = await getClient.get('/ProfileCtrl/ProfileDisplay', {
        params: {
            access_token: accessToken,
            user_id: userId
        }
    })
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

export const uploadAvatar = async (img) => {
    const [accessToken] = await getUserCreds()
    const file = {
        uri: img['uri'],
        name: img['fileName'],
        type: img['type']
    }
    const formData = new FormData()
    formData.append('data', file)
    const response = await getClient.post(
        '/MediaCtrl/ImageUpload?access_token=' + accessToken + '&location=Profile',
        formData
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

export const deleteAccount = async () => {
    const [accessToken] = await getUserCreds()
    const response = await getClient.get(
        '/AccountCtrl/DeleteAccount?access_token=' + accessToken,
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

export const editProfile = async (data) => {
    const [accessToken] = await getUserCreds()
    const formData = new FormData()
    formData.append('nickname', data.username)
    formData.append('birthday', data.birthDate)
    formData.append('residence', data.residence)
    formData.append('about_me', data.freeword)
    formData.append('gender', data.gender)
    formData.append('job', data.job)
    formData.append('personality', data.character)
    for (i in data.hobbies) {
        if (!isNaN(data.hobbies[i])) {
            formData.append('hobby', data.hobbies[i])
        }
    }

    const response = await getClient.post(
        '/ProfileCtrl/ProfileEdit?access_token=' + accessToken,
        formData
    )
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}