import getClient from './getClient'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { UserCreds } from '../constants/Constants'
import syncStorage from 'sync-storage'

export const storeAccessToken = async (accessToken, userId) => {
    try {
        await AsyncStorage.setItem(
            UserCreds.ACCESS_TOKEN_KEY,
            accessToken
        )
        await AsyncStorage.setItem(
            UserCreds.USER_ID_KEY,
            userId.toString()
        )
        syncStorage.set(
            UserCreds.IS_FIRST_TIME_KEY,
            true
        )
        console.log('Store Token : ' + accessToken)
    } catch (error) {
        console.log('Storing Access Token : ' + error)
    }
}

export const postRegister = async (username, email, password) => {
    const response = await getClient.get('/SignUpCtrl/SignUp', {
        params: {
            login_id: email,
            nickname: username,
            password: password,
            language: 'en'
        }
    })
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}

export const postLogin = async (email, password) => {
    const response = await getClient.get('/LoginCtrl/Login', {
        params: {
            login_id: email,
            password: password,
            language: 'en'
        }
    })
    console.log(JSON.stringify(response.data, null, 2))

    return response.data
}