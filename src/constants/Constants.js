export const UserCreds = {
    ACCESS_TOKEN_KEY: 'ACCESS_TOKEN',
    USER_ID_KEY: 'USER_ID',
    IS_FIRST_TIME_KEY: 'IS_FIRST_TIME'
}

export const Genders = [
    { id: 1, gender: 'Male' },
    { id: 2, gender: 'Female' },
]

export const Jobs = [
    { id: 1, job: 'Mobile' },
    { id: 2, job: 'Web' },
    { id: 3, job: 'Backend' },
    { id: 0, job: 'Other' }
]

export const Residences = [
    { residence: 'Bali' },
    { residence: 'Jakarta' },
    { residence: 'Bandung' },
    { residence: 'Other' }
]

export const Hobbies = [
    { id: 1, hobby: 'Gaming' },
    { id: 2, hobby: 'Walking' },
    { id: 3, hobby: 'Swimming' },
    { id: 4, hobby: 'Talking' },
    { id: 0, hobby: 'Other' },
]

export const Characters = [
    { id: 1, character: 'Funny' },
    { id: 2, character: 'Angry' },
    { id: 3, character: 'Boring' },
    { id: 0, character: 'Other' },
]