import { StyleSheet } from 'react-native'
import Colors from './Colors'

const styles = (props) = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  contentWrapper: {
    padding: 16
  },

  btnWrapper: {
    width: '100%',
  },

  welcomeText: {
    marginBottom: 40,
    fontFamily: 'Rubik-Bold',
    alignSelf: 'center',
    color: 'black',
    fontSize: 20,
  },

  loadingIndicator: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
    opacity: 0.6,
    elevation: 10,
  },

  btnMenuAction: {
    color: Colors.accent,
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'right',
  },

  textInput: {
    marginBottom: 16,
    borderColor: 'black',
    borderBottomWidth: 2,
  },

  errorMsg: {
    marginBottom: 16,
    color: 'red',
    fontSize: 12
  },

  sheetContainer: {
    height: 250,
    padding: 16,
    borderRadius: 12,
    backgroundColor: 'white'
  }
})

export default styles