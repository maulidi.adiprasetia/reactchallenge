import { Platform } from "react-native";
import { check, PERMISSIONS, request, RESULTS } from "react-native-permissions";

const PLATFORM_PHOTO_PERMISSION = {
    ios: PERMISSIONS.IOS.PHOTO_LIBRARY,
    android: PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE
}

const PLATFORM_CAMERA_PERMISSION = {
    ios: PERMISSIONS.IOS.CAMERA,
    android: PERMISSIONS.ANDROID.CAMERA
}

const REQUEST_PERMISSION_TYPE = {
    photo: PLATFORM_PHOTO_PERMISSION,
    camera: PLATFORM_CAMERA_PERMISSION
}

export const PERMISSION_TYPE = {
    photo: 'photo',
    camera: 'camera'
}

export const checkPermission = async (type) => {
    const permission = REQUEST_PERMISSION_TYPE[type][Platform.OS]
    if (!permission) return true

    try {
        const result = await check(permission)
        if (result === RESULTS.GRANTED) {
            return result
        }
        return requestPermission(permission)
    } catch (error) {
        console.log(error)
    }
}

export const requestPermission = async (permission) => {
    try {
        const result = await request(permission)
        return result
    } catch (error) {
        console.log(error)
    }
}