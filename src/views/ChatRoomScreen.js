import { useNavigation } from "@react-navigation/core";
import React, { useCallback, useEffect, useState } from "react";
import { FlatList, Image, StyleSheet, Text, TextInput, View } from "react-native";
import { BottomSheet } from "react-native-btr";
import { TouchableOpacity } from "react-native-gesture-handler";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import { checkPermission, PERMISSION_TYPE } from "../actions/AppPermission";
import { showErrorAlertDialog } from "../actions/commonActions";
import CustomButton from "../components/atoms/CustomButton";
import CustomToolbar from "../components/atoms/CustomToolbar";
import Colors from "../constants/Colors";
import styles from "../constants/Styles";
import { getChats, getMoreChats, sendChat, sendImage, uploadImage } from "../services/msgServices";
import { readChats, writeChats, writeMoreChats } from "../storage/ChatRealm";

const chatStyle = StyleSheet.create({
    contentWrapper: {
        marginVertical: 8,
        marginHorizontal: 16,
    },
    footer: {
        padding: 8,
        flexDirection: 'row',
        borderTopWidth: 1,
        alignItems: 'center'
    },
    plusBtn: {
        width: 30,
        height: 30,
    },
    msgInput: {
        flex: 3,
        padding: 8,
        marginHorizontal: 16,
        maxHeight: 70,
        borderWidth: 1,
        borderRadius: 12,
    },
    receiveChat: {
        width: 'auto',
        maxWidth: '80%',
        padding: 8,
        color: 'white',
        backgroundColor: Colors.accent,
        borderRadius: 12,
        alignSelf: 'flex-start'
    },
    sendChat: {
        width: 'auto',
        maxWidth: '80%',
        padding: 8,
        alignSelf: 'flex-end',
        color: 'white',
        backgroundColor: Colors.primary,
        borderRadius: 12
    },
    chatImage: {
        width: 200,
        height: 200
    }
})

const ChatRoomScreen = ({ route }) => {
    const { data } = route.params
    const nav = useNavigation()
    const [chats, setChats] = useState([])
    const [isOpen, setIsOpen] = useState(false)
    const [inputMessage, setInputMessage] = useState('')

    const getChat = useCallback(async () => {
        await getChats(data.receiverId).then(response => {
            if (response['status'] == 1) {
                writeChats(response['items']).then(_ => {
                    setChats(response['items'])
                })
                // setChats(response['items'])
            } else {
                showErrorAlertDialog(
                    response['error']['errorMessage'],
                    false
                )
            }
        }).catch(_ => {
            showErrorAlertDialog(
                'No Internet Connection',
                false
            )
        })
    }, [])

    const getMoreChat = async () => {
        const oldMsg = chats[chats.length - 1]
        await getMoreChats(data.receiverId, oldMsg.messageId).then(response => {
            if (response['status'] == 1 && response['items'] != null) {
                const moreChat = response['items']
                setChats(prevChats => [
                    ...prevChats, ...moreChat
                ])
                // writeMoreChats(moreChat)
            }
        })
    }

    useEffect(() => {
        readChats(data.talkId).then(result => {
            if (result.length > 0) {
                console.log('from realm')
                setChats(result)
            } else {
                console.log('from server')
                getChat()
            }
        })
        // getChat()
    }, [])

    const getTime = (item) => {
        const time = item.split(' ')[1].split(':').slice(0, -1)
        let realTime = ''
        time.forEach((item, index) => {
            if (index != time.length - 1) {
                realTime += item + ':'
            } else {
                realTime += item
            }
        })

        return realTime
    }

    const renderSenderItem = (itemData, index) => {
        return (
            <>
                <View style={[chatStyle.contentWrapper, { flexDirection: 'row' }, { justifyContent: 'flex-end' }]}>
                    <Text style={[{ marginRight: 8 }, { alignSelf: 'flex-end' }]}>{getTime(itemData.time)}</Text>
                    <View style={chatStyle.sendChat}>
                        {
                            itemData.mediaId != 0 ?
                                <>
                                    <TouchableOpacity
                                        onPress={() => nav.navigate('UserProfilePreview', { imageUrl: itemData.mediaUrl })}
                                    >
                                        <Image style={chatStyle.chatImage} source={{ uri: itemData.mediaUrl }} />
                                    </TouchableOpacity>
                                </> :
                                itemData.message != '' &&
                                <Text style={{ color: 'white' }}>{itemData.message}</Text>
                        }
                    </View>
                </View>
            </>
        )
    }

    const renderReceiverItem = (itemData, index) => {
        return (
            <>
                <View style={[chatStyle.contentWrapper, { flexDirection: 'row' }, { justifyContent: 'flex-start' }]}>
                    <View style={chatStyle.receiveChat}>
                        {
                            itemData.mediaId != 0 ?
                                <>
                                    <TouchableOpacity
                                        onPress={() => nav.navigate('UserProfilePreview', { imageUrl: itemData.mediaUrl })}
                                    >
                                        <Image style={chatStyle.chatImage} source={{ uri: itemData.mediaUrl }} />
                                    </TouchableOpacity>
                                    {/* <Text style={chatStyle.receiveChat}>{itemData.message}</Text> */}
                                </> :
                                itemData.message != '' &&
                                <Text style={{ color: 'white' }}>{itemData.message}</Text>
                        }
                    </View>
                    <Text style={[{ marginLeft: 8 }, { alignSelf: 'flex-end' }]}>{getTime(itemData.time)}</Text>
                </View>
            </>
        )
    }

    const renderEmptyItem = () => {
        return (
            <View style={[{ alignItems: 'center' }, { minHeight: 250 }]}>
                <Text>No Data Found</Text>
            </View>
        )
    }

    const handleOnChangeText = (text) => {
        setInputMessage(text)
    }

    const handleOnSend = async () => {
        await sendChat(data.receiverId, inputMessage).then(response => {
            if (response['status'] == 1) {
                getChat()
                setInputMessage('')
            }
        })
    }

    const openCamera = () => {
        checkPermission(PERMISSION_TYPE.camera).then(response => {
            switch (response) {
                case 'granted':
                    launchCamera({ mediaType: 'photo' }, response => {
                        console.log(JSON.stringify(response, null, 2))
                        if (response['didCancel'] != true) {
                            const img = response['assets'][0]
                            setIsOpen(false)
                            uploadImage(img)
                                .then(result => {
                                    if (result['status'] == 1) {
                                        sendImage(data.receiverId, result['imageId']).then(response => {
                                            if (response['status'] == 1) {
                                                getChat()
                                            }
                                        })
                                    } else {
                                        showErrorAlertDialog(
                                            result['error']['errorMessage'],
                                            false
                                        )
                                    }
                                })
                                .catch(error => {
                                    setIsOpen(false)
                                    showErrorAlertDialog(
                                        'Error Occured When Uploading Image',
                                        false
                                    )
                                })
                        }
                    })
                    break
                case 'blocked':
                    setIsOpen(false)
                    showErrorAlertDialog(
                        'Cannot Access Device Camera',
                        false
                    )
                    break
                case 'unavailable':
                    setIsOpen(false)
                    showErrorAlertDialog(
                        'No Camera Available',
                        false
                    )
                    break
            }
        })
    }

    const openGallery = () => {
        checkPermission(PERMISSION_TYPE.photo).then(response => {
            switch (response) {
                case 'granted':
                    launchImageLibrary({ mediaType: 'photo' }, response => {
                        console.log(JSON.stringify(response, null, 2))
                        if (response['didCancel'] != true) {
                            const img = response['assets'][0]
                            setIsOpen(false)
                            uploadImage(img)
                                .then(result => {
                                    if (result['status'] == 1) {
                                        sendImage(data.receiverId, result['imageId']).then(response => {
                                            if (response['status'] == 1) {
                                                getChat()
                                            }
                                        })
                                    } else {
                                        showErrorAlertDialog(
                                            result['error']['errorMessage'],
                                            false
                                        )
                                    }
                                })
                                .catch(error => {
                                    setIsOpen(false)
                                    showErrorAlertDialog(
                                        'Error Occured When Uploading Image',
                                        false
                                    )
                                })
                        }
                    })
                    break
                case 'blocked':
                    setIsOpen(false)
                    showErrorAlertDialog(
                        'Cannot Access Device Gallery',
                        false
                    )
                    break
                case 'unavailable':
                    setIsOpen(false)
                    showErrorAlertDialog(
                        'No Gallery Available',
                        false
                    )
                    break
            }
        })
    }
    
    return (
        <View style={styles.container}>
            <CustomToolbar
                title={data.nickname}
                types='chatRoom'
                imageUrl={data.imageUrl}
                onPressImage={() => {
                    nav.navigate('UserProfilePreview', {
                        imageUrl: data.imageUrl
                    })
                }}
                hasBack={true}
                onPressBack={() => {
                    // writeChats(chats)
                    nav.goBack()
                }}
            />
            <FlatList
                data={chats != null ? chats : []}
                renderItem={({ item, index }) => item.messageKind == 1 ? renderReceiverItem(item, index) : renderSenderItem(item, index)}
                keyExtractor={(item, index) => index.toString()}
                inverted={true}
                ListEmptyComponent={renderEmptyItem()}
                onEndReached={() => {
                    getMoreChat()
                }}
            />
            <View style={chatStyle.footer}>
                <TouchableOpacity
                    onPress={() => setIsOpen(true)}
                >
                    <Image style={chatStyle.plusBtn} source={require('../assets/images/plus.png')} />
                </TouchableOpacity>
                <TextInput
                    style={chatStyle.msgInput}
                    multiline={true}
                    numberOfLines={3}
                    placeholder='Messages'
                    onChangeText={(text) => handleOnChangeText(text)}
                    value={inputMessage}
                // returnKeyType='done'
                // onKeyPress={keyPress => {
                //     if (keyPress.nativeEvent.key == 'Enter') {
                //         console.log('enter')
                //     }
                // }}
                />
                <CustomButton
                    title='Send'
                    types='primary'
                    onPress={() => handleOnSend()}
                />
            </View>
            <BottomSheet
                visible={isOpen}
                onBackButtonPress={() => setIsOpen(false)}
                onBackdropPress={() => setIsOpen(false)}
            >
                <View style={[styles.sheetContainer]}>
                    <View style={{ marginTop: 16 }}>
                        <CustomButton
                            title='Gallery'
                            types='secondary'
                            onPress={openGallery}
                        />
                    </View>
                    <View style={{ marginTop: 16 }}>
                        <CustomButton
                            title='Camera'
                            types='secondary'
                            onPress={openCamera}
                        />
                    </View>
                </View>
            </BottomSheet>
        </View>
    )
}

export default ChatRoomScreen