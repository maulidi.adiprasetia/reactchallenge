import React, { useContext, useState, } from 'react'
import { StyleSheet, Text, View, TextInput, ActivityIndicator, Keyboard } from "react-native"
import styles from "../constants/Styles"
import { Formik } from 'formik'
import * as yup from 'yup'
import CustomButton from '../components/atoms/CustomButton'
import { postLogin, storeAccessToken, storeUserId } from '../services/authServices'
import { useNavigation } from '@react-navigation/core'
import CustomToolbar from '../components/atoms/CustomToolbar'
import CustomLoader from '../components/atoms/CustomLoader'
import { AuthContext } from '../../App'
import { showErrorAlertDialog } from '../actions/commonActions'

const initialValues = {
    email: '',
    password: '',
}

const validationSchema = yup.object().shape({
    email: yup.string()
        .email('Invalid Email Format')
        .max(20, 'Maximum 20 Characters')
        .required('This Field Is Required!'),
    password: yup.string()
        .min(4, 'Minimum 4 Characters')
        .max(10, 'Maximum 10 Characters')
        .required('This Field Is Required!'),
})


const LoginScreen = () => {
    const { signIn } = useContext(AuthContext)
    const [isTappedBtn, setIsTappedBtn] = useState(false)
    const nav = useNavigation()
    return (
        <>
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={values =>
                postLogin(values.email, values.password)
                    .then(response => {
                        if (response['status'] == 1) {
                            storeAccessToken(response['accessToken'], response['userId']).then(result => {
                                signIn()
                            })
                        } else {
                            showErrorAlertDialog(
                                response['error']['errorMessage'],
                                false
                            )
                        }
                    })
                    .catch(error => {
                        showErrorAlertDialog(
                            'No Internet Connection',
                            false
                        )
                    })
            }>
                {({ values, handleChange, handleBlur, handleSubmit, errors, touched, isSubmitting, isValid, }) => (
                    <View style={styles.container}>
                        <CustomToolbar
                            title='Login'
                            hasBack={true}
                            onPressBack={() => nav.goBack()}
                            hasMenuBtn={false} />
                        <View style={[styles.contentWrapper]}>
                            <Text>Email</Text>
                            <TextInput
                                style={[styles.textInput]}
                                value={values.email}
                                onChangeText={handleChange('email')}
                                onBlur={handleBlur('email')}
                                keyboardType='email-address'
                                textContentType='emailAddress'
                                placeholder={'Email'} />
                            {errors.email && isTappedBtn && touched.email &&
                                <Text style={styles.errorMsg}>{errors.email}</Text>
                            }
                            <Text>Password</Text>
                            <TextInput
                                style={[styles.textInput]}
                                value={values.password}
                                onChangeText={handleChange('password')}
                                onBlur={handleBlur('password')}
                                textContentType='password'
                                secureTextEntry={true}
                                placeholder={'Password'} />
                            {errors.password && isTappedBtn && touched.password &&
                                <Text style={styles.errorMsg}>{errors.password}</Text>
                            }
                            <View style={[styles.btnWrapper, { marginTop: 16, }]}>
                                <CustomButton title="Login" disabled={!isValid && isTappedBtn} types='primary' onPress={() => {
                                    setIsTappedBtn(true)
                                    Keyboard.dismiss()
                                    handleSubmit()
                                }} />
                            </View>
                        </View>
                        {
                            isSubmitting &&
                            <CustomLoader />
                        }
                    </View>
                )}
            </Formik>
        </>
    )
}

export default LoginScreen