import React, { useContext, useReducer, useState } from "react";
import { View, Text, StatusBar, Button, StyleSheet, TextInput, ActivityIndicator, Keyboard } from 'react-native'
import styles from '../constants/Styles'
import CustomButton from '../components/atoms/CustomButton'
import { Formik } from "formik";
import * as yup from 'yup';
import { useNavigation } from "@react-navigation/core";
import { postRegister, storeAccessToken } from "../services/authServices";
import CustomToolbar from "../components/atoms/CustomToolbar";
import CustomLoader from "../components/atoms/CustomLoader";
import { AuthContext } from "../../App";
import { showErrorAlertDialog } from "../actions/commonActions";

const initialValues = {
    username: '',
    email: '',
    password: '',
}

const validationSchema = yup.object().shape({
    username: yup.string()
        .max(50, 'Maximum 50 Characters')
        .required('This Field Is Required!'),
    email: yup.string()
        .email('Invalid Email Format')
        .max(20, 'Maximum 20 Characters')
        .required('This Field Is Required!'),
    password: yup.string()
        .min(4, 'Minimum 4 Characters')
        .max(10, 'Maximum 10 Characters')
        .required('This Field Is Required!'),
})

const RegisterScreen = () => {
    const { signIn } = useContext(AuthContext)
    const [isTappedBtn, setIsTappedBtn] = useState(false)
    const nav = useNavigation()
    return (
        <>
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={values =>
                postRegister(values.username, values.email, values.password)
                    .then(response => {
                        if (response['status'] == 1) {
                            storeAccessToken(response['accessToken'], response['userId']).then(result => {
                                signIn()
                            })
                        } else {
                            showErrorAlertDialog(
                                response['error']['errorMessage'],
                                false
                            )
                        }
                    })
                    .catch(error => {
                        showErrorAlertDialog(
                            'No Internet Connection',
                            false
                        )
                    })
            }>
                {({ values, errors, touched, isValid, isSubmitting, handleChange, handleSubmit, handleBlur }) => (
                    <View style={styles.container}>
                        <CustomToolbar
                            title='Register'
                            hasBack={true}
                            onPressBack={() => nav.goBack()}
                            hasMenuBtn={false} />
                        <View style={[styles.contentWrapper]}>
                            <Text>User Name</Text>
                            <TextInput
                                style={[styles.textInput]}
                                value={values.username}
                                onChangeText={handleChange('username')}
                                onBlur={handleBlur('username')}
                                textContentType='username'
                                placeholder='User Name' />
                            {errors.username && touched.username && isTappedBtn &&
                                <Text style={styles.errorMsg}>{errors.username}</Text>
                            }
                            <Text>Email</Text>
                            <TextInput
                                style={[styles.textInput]}
                                value={values.email}
                                onChangeText={handleChange('email')}
                                onBlur={handleBlur('email')}
                                keyboardType='email-address'
                                textContentType='emailAddress'
                                placeholder='Email' />
                            {errors.email && touched.email && isTappedBtn &&
                                <Text style={styles.errorMsg}>{errors.email}</Text>
                            }
                            <Text>Password</Text>
                            <TextInput
                                style={[styles.textInput]}
                                value={values.password}
                                onChangeText={handleChange('password')}
                                onBlur={handleBlur('password')}
                                textContentType='password'
                                secureTextEntry
                                placeholder='Password' />
                            {errors.password && touched.password && isTappedBtn &&
                                <Text style={styles.errorMsg}>{errors.password}</Text>
                            }
                            <View style={[styles.btnWrapper, { marginTop: 16, }]}>
                                <CustomButton title='Register' disabled={!isValid && isTappedBtn} types='primary' onPress={() => {
                                    setIsTappedBtn(true)
                                    Keyboard.dismiss()
                                    handleSubmit()
                                }} />
                            </View>
                        </View>
                        {
                            isSubmitting &&
                            <CustomLoader />
                        }
                    </View>
                )}
            </Formik>
        </>
    )
}

export default RegisterScreen