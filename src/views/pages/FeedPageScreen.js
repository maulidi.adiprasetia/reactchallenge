import { useIsFocused, useNavigation } from "@react-navigation/core";
import React, { useCallback, useEffect, useState } from "react";
import { ActivityIndicator, FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { showErrorAlertDialog } from "../../actions/commonActions";
import CustomToolbar from "../../components/atoms/CustomToolbar";
import { emptyBox } from "../../components/atoms/LottieFiles";
import { feedShimmer, feedShimmerEndList } from "../../components/atoms/Shimmer";
import Colors from "../../constants/Colors";
import styles from "../../constants/Styles";
import { getFeed, getMoreFeed } from "../../services/feedServices";

const feedStyle = StyleSheet.create({
    feedImage: {
        width: 150,
        height: 150,
        borderRadius: 75,
        marginBottom: 16
    },
    feedName: {
        color: 'black',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
    }
})

const FeedPage = ({ route }) => {
    const isFocused = useIsFocused()
    const nav = useNavigation()
    const [feeds, setFeeds] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [isFetchMore, setIsFetchMore] = useState(false)
    const [isEndData, setIsEndData] = useState(false)
    const [goToDetail, setGoToDetail] = useState(false)
    let lastLoginTime = ''

    const getFeeds = useCallback(async () => {
        setIsEndData(false)
        try {
            await getFeed().then(response => {
                if (response['status'] == 1) {
                    lastLoginTime = response['lastLoginTime']
                    setFeeds([
                        ...feeds, ...response['items']
                    ])
                } else {
                    showErrorAlertDialog(
                        response['error']['errorMessage'],
                        true,
                        () => getFeeds(),
                        null,
                        'Retry'
                    )
                }
            })
        } catch (error) {
            showErrorAlertDialog(
                'No Internet Connection',
                false
            )
        }
    }, [])

    const getMoreFeeds = useCallback(async () => {
        try {
            await getMoreFeed(lastLoginTime).then(response => {
                if (response['status'] == 1) {
                    lastLoginTime = response['lastLoginTime']
                    const moreFeeds = response['items']
                    setFeeds(prevFeeds => [
                        ...prevFeeds, ...moreFeeds
                    ])
                } else {
                    setIsEndData(true)
                    showErrorAlertDialog(
                        response['error']['errorMessage'],
                        false
                    )
                }
            })
        } catch (error) {
            showErrorAlertDialog(
                'No Internet Connection',
                false
            )
        }
    }, [])

    useEffect(() => {
        setIsLoading(true)
        if (isFocused && !goToDetail) {
            getFeeds().then(_ => {
                setIsLoading(false)
            })
        } else if (isFocused) {
            setIsLoading(false)
            setGoToDetail(false)
        }
    }, [isFocused])

    const feedRenderItem = (itemData, index) => {
        return (
            <TouchableOpacity onPress={() => {
                setGoToDetail(true)
                nav.navigate('UserDetail', {
                    data: itemData
                })
            }}>
                <View style={[styles.contentWrapper]}>
                    <Image style={[feedStyle.feedImage]} source={
                        itemData.imageUrl == null ?
                            require('../../assets/images/profile.png') :
                            { uri: itemData.imageUrl }
                    } />
                    <Text style={feedStyle.feedName}>{itemData.nickname}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <View style={[styles.container]}>
            <CustomToolbar
                title="TrainingApps"
                hasBack={false}
                hasMenuBtn={false}
            />
            <View style={[{ alignItems: 'center' }]}>
                <SafeAreaView>
                    {
                        isLoading && !goToDetail ?
                            feedShimmer() :
                            <FlatList
                                data={feeds}
                                renderItem={({ item, index }) => feedRenderItem(item, index)}
                                keyExtractor={(data, index) => index.toString()}
                                numColumns={2}
                                refreshing={isLoading && !goToDetail}
                                onRefresh={() => {
                                    setIsLoading(true)
                                    getFeeds().then(_ => {
                                        setIsLoading(false)
                                    })
                                }}
                                ListEmptyComponent={!isLoading && emptyBox()}
                                scrollEnabled={true}
                                onEndReached={() => {
                                    if (!isFetchMore && !isEndData) {
                                        setIsFetchMore(true)
                                        getMoreFeeds().then(result => {
                                            setIsFetchMore(false)
                                        })
                                    }
                                }}
                                onEndReachedThreshold={2.5}
                                ListFooterComponent={() => (
                                    isFetchMore &&
                                    feedShimmerEndList()
                                )}
                            />
                    }
                    <View style={{ minHeight: 70 }}></View>
                </SafeAreaView>
            </View>
        </View>
    )
}

export default FeedPage