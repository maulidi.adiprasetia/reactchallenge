import React, { createRef, useCallback, useContext, useEffect, useMemo, useRef, useState } from "react";
import { Animated, Image, PermissionsAndroid, ScrollView, StyleSheet, Text, TouchableHighlight, View } from "react-native";
import DialogManager from "react-native-dialog-component";
import { AuthContext } from "../../../App";
import { showErrorAlertDialog } from "../../actions/commonActions";
import CustomButton from "../../components/atoms/CustomButton";
import CustomToolbar from "../../components/atoms/CustomToolbar";
import styles from "../../constants/Styles";
import { deleteAccount, deleteAvatar, getProfile, uploadAvatar } from "../../services/profileServices";
import { createShimmerPlaceholder } from 'react-native-shimmer-placeholder'
import LinearGradient from 'react-native-linear-gradient'
import { BottomSheet } from 'react-native-btr'
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import { checkPermission, PERMISSION_TYPE } from "../../actions/AppPermission";
import { useIsFocused, useNavigation } from "@react-navigation/core";
import { closeAlert } from "react-native-customisable-alert";
import CustomLoader from "../../components/atoms/CustomLoader";

const myPageStyle = StyleSheet.create({
    profileWrapper: {
        flexDirection: 'row',
        marginBottom: 24,
        alignItems: 'center'
    },

    profileImg: {
        width: 100,
        height: 100,
        borderRadius: 25,
        marginRight: 24
    },

    profileInformation: {
        marginBottom: 4,
        fontSize: 16,
        fontWeight: 'bold'
    },

    aboutWrapper: {
        marginBottom: 24
    },

    aboutTitle: {
        fontSize: 24,
        fontWeight: 'bold'
    },

    aboutDesc: {
        width: '100%',
        minHeight: 200,
        fontWeight: '500',
        textAlign: 'justify',
    },
})

const ShimmerPlaceholder = createShimmerPlaceholder(LinearGradient)

const MyPage = () => {
    const { signOut } = useContext(AuthContext)
    const nav = useNavigation()
    const isFocused = useIsFocused()

    const [data, setData] = useState({
        email: 'Email Not Found',
        password: 'Password Not Found',
        about: 'There is Nothing About This User',
        avatarUrl: null,
        avatarId: null,
        visible: false,
        isOpen: false,
        username: '',
        birth: '',
        hobbies: '',
        personality: null,
        residence: '',
        job: null,
        gender: null,
        isLoading: false,
    })

    const getUser = useCallback(async () => {
        try {
            getProfile().then(response => {
                setData({
                    ...data,
                    email: response['email'],
                    password: response['password'],
                    about: response['aboutMe'],
                    avatarUrl: response['imageUrl'],
                    avatarId: response['imageId'],
                    visible: true,
                    username: response['nickname'],
                    birth: response['birthday'],
                    hobbies: response['hobby'],
                    personality: response['personality'],
                    residence: response['residence'],
                    job: response['job'],
                    gender: response['gender'],
                })
            })
        } catch (error) {
            console.log(error)
        }
    }, [])

    useEffect(() => {
        setData({
            ...data,
            visible: false
        })
        if (isFocused) {
            getUser()
        }
    }, [isFocused])

    const options = {
        mediaType: 'photo',
    }

    const openCamera = () => {
        checkPermission(PERMISSION_TYPE.camera).then(response => {
            switch (response) {
                case 'granted':
                    launchCamera(options, response => {
                        console.log(JSON.stringify(response, null, 2))
                        if (response['didCancel'] != true) {
                            const img = response['assets'][0]
                            uploadAvatar(img)
                                .then(result => {
                                    if (result['status'] == 1) {
                                        setData({
                                            ...data,
                                            isOpen: false,
                                            avatarUrl: img['uri'],
                                        })
                                    } else {
                                        showErrorAlertDialog(
                                            result['error']['errorMessage'],
                                            false
                                        )
                                    }
                                })
                                .catch(error => {
                                    setData({ ...data, isOpen: false })
                                    showErrorAlertDialog(
                                        'No Internet Connection',
                                        false
                                    )
                                })
                        }
                    })
                    break
                case 'blocked':
                    setData({ ...data, isOpen: false })
                    showErrorAlertDialog(
                        'Cannot Access Device Camera',
                        false
                    )
                    break
                case 'unavailable':
                    setData({ ...data, isOpen: false })
                    showErrorAlertDialog(
                        'No Camera Available',
                        false
                    )
                    break
            }
        })
    }

    const openGallery = () => {
        checkPermission(PERMISSION_TYPE.photo).then(response => {
            switch (response) {
                case 'granted':
                    launchImageLibrary(options, response => {
                        console.log(JSON.stringify(response, null, 2))
                        if (response['didCancel'] != true) {
                            const img = response['assets'][0]
                            setData({
                                ...data,
                                isLoading: true,
                                isOpen: false,
                            })
                            uploadAvatar(img)
                                .then(result => {
                                    if (result['status'] == 1) {
                                        setData({
                                            ...data,
                                            isOpen: false,
                                            isLoading: false,
                                            avatarUrl: img['uri']
                                        })
                                    } else {
                                        showErrorAlertDialog(
                                            result['error']['errorMessage'],
                                            false
                                        )
                                    }
                                })
                                .catch(error => {
                                    setData({ ...data, isOpen: false })
                                    showErrorAlertDialog(
                                        'No Internet Connection',
                                        false
                                    )
                                })
                        }
                    })
                    break
                case 'blocked':
                    setData({ ...data, isOpen: false })
                    showErrorAlertDialog(
                        'Cannot Access Device Gallery',
                        false
                    )
                    break
                case 'unavailable':
                    setData({ ...data, isOpen: false })
                    showErrorAlertDialog(
                        'No Gallery Available',
                        false
                    )
                    break
            }
        })
    }

    return (
        <View style={[styles.container]}>
            <CustomToolbar
                title="TrainingApps"
                hasBack={false}
                hasMenuBtn={true}
                menuBtn={
                    <Text style={styles.btnMenuAction}>Logout</Text>
                }
                onPressMenu={() =>
                    showErrorAlertDialog(
                        'Are you sure to exit?',
                        true,
                        () => {
                            signOut().then(response => {
                                closeAlert()
                            })
                        },
                        null,
                        'OK'
                    )
                }
            />
            <ScrollView>
                <View style={[styles.contentWrapper]}>
                    <View style={[myPageStyle.profileWrapper]}>
                        <ShimmerPlaceholder visible={data.visible} style={myPageStyle.profileImg}>
                            <TouchableHighlight
                                onPress={() =>
                                    setData({ ...data, isOpen: !data.isOpen })
                                }
                            >
                                <Image style={[myPageStyle.profileImg]} source={
                                    data.avatarUrl != null
                                        ? { uri: data.avatarUrl }
                                        : require('../../assets/images/profile.png')
                                } />
                            </TouchableHighlight>
                        </ShimmerPlaceholder>
                        <View style={[myPageStyle.profile]}>
                            <ShimmerPlaceholder visible={data.visible} style={myPageStyle.profileInformation}>
                                <Text style={[myPageStyle.profileInformation]}>{data.email}</Text>
                            </ShimmerPlaceholder>
                            <ShimmerPlaceholder visible={data.visible} style={myPageStyle.profileInformation}>
                                <Text style={[myPageStyle.profileInformation]}>{data.password}</Text>
                            </ShimmerPlaceholder>
                            {
                                data.visible &&
                                <CustomButton
                                    title='Edit Profile'
                                    types='secondary'
                                    onPress={() => {
                                        nav.navigate('EditProfile', {
                                            data: data
                                        })
                                    }} />
                            }
                        </View>
                    </View>
                    <View style={[myPageStyle.aboutWrapper]}>
                        <Text style={[myPageStyle.aboutTitle]}>About Me</Text>
                        <ShimmerPlaceholder visible={data.visible} style={[{ width: '100%' }]}>
                            <Text style={[myPageStyle.aboutDesc]}>
                                {'\t\t\t'}{data.about}
                            </Text>
                        </ShimmerPlaceholder>
                    </View>
                    <View style={[{ marginBottom: 16 }]}>
                        {
                            data.visible &&
                            <CustomButton
                                title='Term & Condition'
                                types='primary'
                                onPress={() => {
                                    nav.navigate('TermsCondition')
                                }} />
                        }
                    </View>
                    {
                        data.visible &&
                        <CustomButton
                            title='Delete Account'
                            types='danger'
                            onPress={() =>
                                showErrorAlertDialog(
                                    'Are you sure to delete account?',
                                    true,
                                    () => deleteAccount().then(response => {
                                        if (response['status'] == 1) {
                                            signOut()
                                            DialogManager.destroy()
                                        } else {
                                            showErrorAlertDialog(
                                                response['error']['errorMessage'],
                                                false
                                            )
                                        }
                                    }).catch(error => {
                                        showErrorAlertDialog(
                                            'No Internet Connection',
                                            false
                                        )
                                    }),
                                    null,
                                    'OK'
                                )
                            }
                        />
                    }
                </View>
            </ScrollView>
            <BottomSheet
                visible={data.isOpen}
                onBackButtonPress={() => setData({ ...data, isOpen: !data.isOpen })}
                onBackdropPress={() => setData({ ...data, isOpen: !data.isOpen })}
            >
                <View style={[styles.sheetContainer]}>
                    <View style={{ marginTop: 16 }}>
                        <CustomButton
                            title='Gallery'
                            types='secondary'
                            onPress={openGallery}
                        />
                    </View>
                    <View style={{ marginTop: 16 }}>
                        <CustomButton
                            title='Camera'
                            types='secondary'
                            onPress={openCamera}
                        />
                    </View>
                    {
                        data.avatarUrl != null &&
                        <View style={{ marginTop: 16 }}>
                            <CustomButton
                                title='Delete'
                                types='danger'
                                onPress={() => deleteAvatar(data.avatarId).then(result => {
                                    if (result['status'] == 1) {
                                        setData({
                                            ...data,
                                            isOpen: false,
                                            avatarUrl: null,
                                        })
                                    } else {
                                        showErrorAlertDialog(
                                            result['error']['errorMessage'],
                                            false
                                        )
                                    }
                                }).catch(error => {
                                    showErrorAlertDialog(
                                        'No Internet Connection',
                                        false
                                    )
                                })}
                            />
                        </View>
                    }
                </View>
            </BottomSheet>
            {
                data.isLoading &&
                <CustomLoader />
            }
        </View>
    )
}

export default MyPage