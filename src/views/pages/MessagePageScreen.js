import AsyncStorage from "@react-native-async-storage/async-storage";
import { useIsFocused, useNavigation } from "@react-navigation/core";
import React, { useCallback, useEffect, useState } from "react";
import { ActivityIndicator, FlatList, Image, StyleSheet, Text, View } from "react-native";
import { CheckBox } from "react-native-elements";
import { TouchableOpacity } from "react-native-gesture-handler";
import syncStorage from "sync-storage";
import { showErrorAlertDialog } from "../../actions/commonActions";
import CustomButton from "../../components/atoms/CustomButton";
import CustomLoader from "../../components/atoms/CustomLoader";
import CustomToolbar from "../../components/atoms/CustomToolbar";
import { emptyBox } from "../../components/atoms/LottieFiles";
import { messageShimmer } from "../../components/atoms/Shimmer";
import Colors from "../../constants/Colors";
import { UserCreds } from "../../constants/Constants";
import styles from "../../constants/Styles";
import { useCancelToken } from "../../services/cancelRequest";
import { deleteMessages, getMessages } from "../../services/msgServices";
import { readMessages, removeMessages, writeMessages } from "../../storage/MessageRealm";

const msgStyle = StyleSheet.create({
    container: {
        margin: 16,
        paddingBottom: 16,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
    },
    image: {
        width: 80,
        height: 80,
        borderRadius: 40,
        marginRight: 16
    },
    username: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    message: {
        fontSize: 16
    }
})

const MessagePage = () => {
    const isFocused = useIsFocused()
    const nav = useNavigation()
    const first = syncStorage.get(UserCreds.IS_FIRST_TIME_KEY)
    const [isEdit, setIsEdit] = useState(false)
    const [isFetching, setIsFetching] = useState(!first ? false : true)
    const [isLoading, setIsLoading] = useState(false)
    const [isEmpty, setIsEmpty] = useState(false)
    const [messages, setMessages] = useState([])

    const deleteItem = async (messages) => {
        setIsLoading(true)
        const selectedMsg = messages.filter(item => {
            return item.checked === true
        })

        await deleteMessages(selectedMsg).then(response => {
            setIsLoading(false)
            if (response['status'] == 1) {
                // setIsFetching(true)
                setIsEdit(false)
                removeMessages(selectedMsg).then(_ => {
                    getMessage()
                })
            } else {
                showErrorAlertDialog(
                    response['error']['errorMessage'],
                    false
                )
            }
        }).catch(error => {
            // if (isCancel(error)) {
            //     console.log('cancel ges')
            //     return
            // }
            showErrorAlertDialog(
                'No Internet Connection',
                false
            )
        })
    }

    const renderItem = (itemData) => {
        return (
            <TouchableOpacity
                onPress={() => {
                    nav.navigate('ChatRoom', {
                        data: {
                            receiverId: itemData.toUserId,
                            nickname: itemData.nickname,
                            imageUrl: itemData.imageUrl,
                            talkId: itemData.talkId,
                        }
                    })
                }}
            >
                <View style={[msgStyle.container]}>
                    <Image style={msgStyle.image} source={
                        itemData.imageUrl != null ?
                            { uri: itemData.imageUrl } :
                            require('../../assets/images/profile.png')
                    } />
                    <View>
                        <Text style={msgStyle.username}>{itemData.nickname}</Text>
                        <Text numberOfLines={1} ellipsizeMode='tail' style={[msgStyle.message, { maxWidth: 250 }]}>{itemData.message}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    const renderEditItem = (itemData, index) => {
        return (
            <View style={[msgStyle.container]}>
                <CheckBox
                    checked={itemData.checked}
                    onIconPress={() => {
                        const listMessage = [...messages]
                        listMessage[index].checked = !itemData.checked
                        setMessages(listMessage)
                    }}
                />
                <Image style={msgStyle.image} source={
                    itemData.imageUrl != null ?
                        { uri: itemData.imageUrl } :
                        require('../../assets/images/profile.png')
                } />
                <View>
                    <Text style={msgStyle.username}>{itemData.nickname}</Text>
                    <Text numberOfLines={1} ellipsizeMode='tail' style={[msgStyle.message, { maxWidth: 150 }]}>{itemData.message}</Text>
                </View>
            </View>
        )
    }

    const getMessage = useCallback(async () => {
        syncStorage.set(UserCreds.IS_FIRST_TIME_KEY, false)
        await getMessages().then(response => {
            if (response['status'] == 1) {
                const responseData = response['items'].map(item => {
                    return ({ ...item, checked: false })
                })
                writeMessages(responseData).then(_ => {
                    setMessages(responseData)
                    setIsEmpty(false)
                })
            } else if (response['error']['errorCode'] == 2) {
                setIsEmpty(true)
                setMessages([])
            } else {
                showErrorAlertDialog(
                    response['error']['errorMessage'],
                    false
                )
            }
        }).catch(error => {
            showErrorAlertDialog(
                'No Internet Connection, Connect to Internet to Get Latest Data',
                false
            )
        })
    }, [])

    useEffect(() => {
        setIsEdit(false)
        setIsLoading(false)
        readMessages().then(response => {
            if (isFocused && !first) {
                console.log('realm true')
                setMessages(response)
                getMessage()
            } else if (isFocused) {
                setIsFetching(true)
                console.log('first time')
                getMessage().then(_ => {
                    setIsFetching(false)
                })
            }
        })
    }, [isFocused])

    return (
        <View style={[styles.container]}>
            <CustomToolbar
                title="TrainingApps"
                hasBack={false}
                hasMenuBtn={isEmpty ? false : true}
                menuBtn={
                    isEdit ?
                        <Text style={styles.btnMenuAction}>Cancel</Text> :
                        <Text style={styles.btnMenuAction}>Edit</Text>
                }
                onPressMenu={() => {
                    if (isEdit) {
                        messages.map(item => {
                            item.checked = false
                        })
                    }
                    setIsEdit(!isEdit)
                    setIsLoading(false)
                }}
            />
            {
                isFetching ?
                    messageShimmer() :
                    <FlatList
                        data={messages}
                        renderItem={({ item, index }) => !isEdit ? renderItem(item) : renderEditItem(item, index)}
                        keyExtractor={(item, index) => index.toString()}
                        refreshing={isFetching}
                        onRefresh={() => {
                            // setIsFetching(true)
                            setIsEdit(false)
                            getMessage()
                        }}
                        ListEmptyComponent={emptyBox()}
                        scrollEnabled={true}
                    />
            }
            {
                isEdit &&
                <View style={styles.contentWrapper}>
                    <CustomButton
                        types='dangerPrimary'
                        title='Delete'
                        loading={isLoading}
                        onPress={() => deleteItem(messages)}
                        disabled={messages.filter(item => item.checked).length == 0}
                    />
                </View>
            }
        </View>
    )
}

export default MessagePage