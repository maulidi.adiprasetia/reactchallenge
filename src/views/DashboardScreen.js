import 'react-native-gesture-handler'
import React, { useContext } from 'react'
import { View, Text, StyleSheet } from "react-native"
import Colors from "../constants/Colors"
import MyPage from './pages/MyPageScreen'
import FeedPage from './pages/FeedPageScreen'
import MessagePage from './pages/MessagePageScreen'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { useNavigation } from '@react-navigation/core'

const tabs = createMaterialBottomTabNavigator()

const DashboardScreen = () => {
    const nav = useNavigation()
    return (
        <>
            <View style={[{ flex: 1 }]}>
                <tabs.Navigator
                    initialRouteName='Feed'
                    activeColor={Colors.accent}
                    barStyle={[{ backgroundColor: 'white' }, { elevation: 5 }, { borderTopWidth: 1 }]}
                    backBehavior='none'
                >
                    <tabs.Screen
                        name='Feed'
                        component={FeedPage}
                        options={{
                            tabBarLabel: 'Feed',
                            tabBarIcon: ({ color }) => (
                                <Icon name='images' color={color} size={20} />
                            )
                        }}
                    ></tabs.Screen>
                    <tabs.Screen
                        name='Message'
                        component={MessagePage}
                        options={{
                            tabBarLabel: 'Message',
                            tabBarIcon: ({ color }) => (
                                <Icon name='comment-alt' color={color} size={20} solid />
                            )
                        }}
                    ></tabs.Screen>
                    <tabs.Screen
                        name='MyPage'
                        component={MyPage}
                        options={{
                            tabBarLabel: 'MyPage',
                            tabBarIcon: ({ color }) => (
                                <Icon name='user' color={color} size={20} />
                            ),
                        }}
                    ></tabs.Screen>
                </tabs.Navigator>
            </View>
        </>
    )
}

export default DashboardScreen