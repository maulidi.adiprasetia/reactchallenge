import React from "react";
import ReactNativeZoomableView from '@dudigital/react-native-zoomable-view/src/ReactNativeZoomableView';
import { Image, StyleSheet, View } from "react-native";
import CustomToolbar from "../components/atoms/CustomToolbar";
import { useNavigation } from "@react-navigation/core";

const profilePreview = StyleSheet.create({
    image: {
        width: '100%',
        height: 300,
        resizeMode: 'contain'
    }
})

const UserProfilePreview = ({ route }) => {
    const { imageUrl } = route.params
    const nav = useNavigation()

    return (
        <View style={[{ flex: 1 }, { backgroundColor: 'black' }]}>
            <CustomToolbar
                title=""
                types='dark'
                hasBack={true}
                onPressBack={() => { nav.goBack() }}
                hasMenuBtn={false}
            />
            <ReactNativeZoomableView
                maxZoom={2}
                minZoom={1}
            >
                <Image style={profilePreview.image} source={
                    imageUrl != null ?
                        { uri: imageUrl } :
                        require('../assets/images/profile.png')
                } />
            </ReactNativeZoomableView>
        </View>
    )
}

export default UserProfilePreview