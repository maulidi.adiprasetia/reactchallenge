import { useNavigation } from "@react-navigation/core";
import React from "react";
import WebView from "react-native-webview";
import CustomToolbar from "../components/atoms/CustomToolbar";

const TermsConditionScreen = () => {
    const nav = useNavigation()
    return (
        <>
            <CustomToolbar
                title='Terms & Condition'
                hasBack={true}
                onPressBack={() => {
                    nav.goBack()
                }}
                hasMenuBtn={false}
            />
            <WebView source={{ uri: 'https://reactnative.dev/' }} />
        </>
    )
}

export default TermsConditionScreen