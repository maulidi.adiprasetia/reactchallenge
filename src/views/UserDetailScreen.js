import { useNavigation } from "@react-navigation/core";
import React, { useCallback, useEffect, useState } from "react";
import { Image, ScrollView, StyleSheet, Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { showErrorAlertDialog } from "../actions/commonActions";
import CustomButton from "../components/atoms/CustomButton";
import CustomToolbar from "../components/atoms/CustomToolbar";
import { userDetailShimmer } from "../components/atoms/Shimmer";
import { Genders, Hobbies, Jobs } from "../constants/Constants";
import styles from "../constants/Styles";
import { getProfileDetail } from "../services/profileServices";

const detailStyle = StyleSheet.create({
    profileImg: {
        position: 'relative',
        width: '100%',
        height: 250,
    },
    username: {
        position: 'absolute',
        bottom: 16,
        left: 16,
        fontSize: 20,
    },
    aboutMe: {
        padding: 16,
        textAlign: 'justify',
        fontSize: 16,
        fontWeight: 'bold'
    },
    row: {
        marginBottom: 16,
        flexDirection: 'row',
    },
    content: {
        width: '50%',
        fontSize: 16,
    },
    btnSendMsg: {
        padding: 16,
    }
})

const UserDetailScreen = ({ route }) => {
    const { data } = route.params
    const nav = useNavigation()

    const [isLoading, setIsLoading] = useState(true)
    const [userData, setUserData] = useState({
        imageUrl: '',
        nickname: '',
        freeword: '',
        gender: null,
        hobby: '',
        age: null,
        job: null,
        userId: null,
    })

    const getHobbiesName = (response) => {
        const selectedHobbyId = response['hobby'].split(',').map(item => {
            return parseInt(item)
        })
        let hobbyName = ''
        const hobbies = Hobbies.filter(item => {
            return selectedHobbyId.includes(item.id)
        })
        hobbies.forEach((item, index) => {
            if (index != hobbies.length - 1) {
                hobbyName += item.hobby + ', '
            } else {
                hobbyName += item.hobby
            }
        })

        return hobbyName
    }

    const getUserDetail = useCallback(async () => {
        await getProfileDetail(data.userId).then(response => {
            if (response['status'] == 1) {
                setUserData({
                    ...userData,
                    imageUrl: response['imageUrl'],
                    nickname: response['nickname'],
                    freeword: response['aboutMe'],
                    gender: Genders.find(item => {
                        return item.id === response['gender']
                    }),
                    hobby: getHobbiesName(response),
                    age: response['age'],
                    job: Jobs.find(item => {
                        return item.id === response['job']
                    }),
                    userId: response['userId']
                })
            } else {
                showErrorAlertDialog(
                    response['error']['errorMessage'],
                    false
                )
            }
        }).catch(error => {
            showErrorAlertDialog(
                'No Internet Connection',
                false
            )
        })
    }, [])

    useEffect(() => {
        getUserDetail().then(_ => {
            setIsLoading(false)
        })
    }, [])

    return (
        <View style={styles.container}>
            <CustomToolbar
                title='Profile Display'
                hasBack={true}
                onPressBack={() => { nav.goBack() }}
                hasMenuBtn={false}
            />
            {
                isLoading ?
                    userDetailShimmer() :
                    <>
                        <View>
                            <TouchableOpacity
                                onPress={() => userData.imageUrl != null && nav.navigate('UserProfilePreview', {
                                    imageUrl: userData.imageUrl
                                })}
                            >
                                <Image style={[detailStyle.profileImg, { resizeMode: userData.imageUrl != null ? 'cover' : 'contain' }]} source={
                                    userData.imageUrl != null ?
                                        { uri: userData.imageUrl } :
                                        require('../assets/images/profile.png')
                                } />
                            </TouchableOpacity>
                            <Text style={[detailStyle.username, { color: userData.imageUrl != null ? 'white' : 'black' }]}>{userData.nickname}</Text>
                        </View>
                        <ScrollView>
                            <View style={styles.contentWrapper}>
                                {
                                    userData.freeword != null &&
                                    <Text style={detailStyle.aboutMe}>{userData.freeword}</Text>
                                }
                                {
                                    userData.gender != null &&
                                    <View style={detailStyle.row}>
                                        <Text style={detailStyle.content}>Sex</Text>
                                        <Text style={detailStyle.content}>{userData.gender.gender}</Text>
                                    </View>
                                }
                                {
                                    userData.hobby != '' &&
                                    <View style={detailStyle.row}>
                                        <Text style={detailStyle.content}>Hobby</Text>
                                        <Text style={detailStyle.content}>{userData.hobby}</Text>
                                    </View>
                                }
                                {
                                    userData.age != null &&
                                    <View style={detailStyle.row}>
                                        <Text style={detailStyle.content}>Age</Text>
                                        <Text style={detailStyle.content}>{userData.age}</Text>
                                    </View>
                                }
                                {
                                    userData.job != null &&
                                    <View style={detailStyle.row}>
                                        <Text style={detailStyle.content}>Occupation</Text>
                                        <Text style={detailStyle.content}>{userData.job.job}</Text>
                                    </View>
                                }
                            </View>
                        </ScrollView>
                        <View style={detailStyle.btnSendMsg}>
                            <CustomButton
                                types='primary'
                                title='Send Message'
                                onPress={() => {
                                    nav.navigate('ChatRoom', {
                                        data: {
                                            receiverId: userData.userId,
                                            nickname: userData.nickname,
                                            imageUrl: userData.imageUrl,
                                            talkId: null,
                                        }
                                    })
                                }}
                            />
                        </View>
                    </>
            }
        </View>
    )
}

export default UserDetailScreen