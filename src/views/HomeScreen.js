import 'react-native-gesture-handler'
import React from 'react'
import { View, Text, StatusBar, Button, StyleSheet } from 'react-native'
import Colors from '../constants/Colors'
import styles from '../constants/Styles'
import CustomButton from '../components/atoms/CustomButton'
import { useNavigation } from '@react-navigation/core'

const homeStyles = StyleSheet.create({
    container: {
        padding: 16,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
})

const HomeScreen = () => {
    const nav = useNavigation()
    return (
        <>
            <StatusBar backgroundColor={Colors.accent} />
            <View style={[homeStyles.container, styles.container]}>
                <Text style={styles.welcomeText}>Training App</Text>
                <View style={[styles.btnWrapper, { marginBottom: 16 }]}>
                    <CustomButton title='Login' types='primary' onPress={() => nav.navigate('Login')} />
                </View>
                <View style={styles.btnWrapper}>
                    <CustomButton title='Register' types='secondary' onPress={() => nav.navigate('Register')} />
                </View>
            </View>
        </>
    )
}

export default HomeScreen
