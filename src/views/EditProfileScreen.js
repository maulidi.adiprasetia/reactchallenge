import { useNavigation } from "@react-navigation/core";
import React, { useEffect, useReducer, useRef, useState } from "react";
import { FlatList, Keyboard, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { TextInput, TouchableHighlight } from "react-native-gesture-handler";
import CustomToolbar from "../components/atoms/CustomToolbar";
import styles from "../constants/Styles";
import { Dropdown } from 'react-native-element-dropdown'
import { Characters, Genders, Hobbies, Jobs, Residences } from "../constants/Constants";
import DialogManager, { ScaleAnimation, DialogContent } from 'react-native-dialog-component';
import { editProfile } from "../services/profileServices";
import { showErrorAlertDialog } from "../actions/commonActions";
import CustomLoader from '../components/atoms/CustomLoader';
import Colors from "../constants/Colors";
import DateTimePicker from "react-native-modal-datetime-picker";
import CustomButton from "../components/atoms/CustomButton";
import Modal from 'react-native-modal';
import moment from "moment";
import { closeAlert } from "react-native-customisable-alert";

const editStyles = StyleSheet.create({
    textLabel: {
        fontWeight: 'bold'
    },

    placeholderText: {
        color: 'grey',
        fontSize: 14
    },

    selectedText: {
        color: 'black'
    }
})

const dialogStyle = StyleSheet.create({
    container: {
        padding: 12,
        minWidth: '80%',
        minHeight: '50%',
        backgroundColor: 'white'
    },
    dialogTitle: {
        marginBottom: 16,
        fontSize: 20,
        fontWeight: 'bold',
        alignSelf: 'center'
    },
    itemContainer: {
        padding: 8,
        marginBottom: 8,
        borderRadius: 8,
    },
    item: {
        color: 'white'
    }
})

const reducer = (prevState, { type, id, payload, isValid = true, errorMsg }) => {
    if (type == 'INPUT') {
        return {
            ...prevState,
            [id]: payload,
            isValid: isValid,
            errorUsername: errorMsg
        }
    }
}

const regex = /^[0-9a-zA-Z(\-)\s]+$/

const convertHobbies = (hobbies) => {
    const stringHobbies = hobbies.split(',')
    return stringHobbies.map(item => {
        return parseInt(item)
    })
}

const EditProfileScreen = ({ route }) => {
    const nav = useNavigation()
    const { data } = route.params
    const [isDatePickerVisible, setIsDatePickerVisible] = useState(false)
    const [selectedHobby, setSelectedHobby] = useState([])
    const [isHobbyVisible, setIsHobbyVisible] = useState(false)

    const initialValue = {
        username: data.username,
        birthDate: data.birth,
        gender: data.gender,
        job: data.job,
        residence: data.residence,
        hobbies: convertHobbies(data.hobbies),
        character: data.personality,
        freeword: data.about,
        isLoading: false,
        isEdited: false,
        isValid: true,
        errorUsername: '',
    }

    const [state, dispatch] = useReducer(reducer, initialValue)

    const getHobbiesName = () => {
        const selectedHobbies = Hobbies.filter((item) => {
            return state.hobbies.includes(item.id)
        })
        let hobbyName = ''
        selectedHobbies.forEach((item, index) => {
            if (index != selectedHobbies.length - 1) {
                hobbyName += item.hobby + ', '
            } else {
                hobbyName += item.hobby
            }
        })

        console.log(hobbyName)
        return hobbyName
    }

    useEffect(() => {
        setSelectedHobby(state.hobbies)
        getHobbiesName()
    }, [])

    const handleHobbyClicked = (data) => {
        dispatch({ type: 'INPUT', id: 'isEdited', payload: true })
        if (!state.hobbies.includes(data.item.id)) {
            setSelectedHobby([
                ...selectedHobby,
                data.item.id
            ])
            dispatch({ type: 'INPUT', id: 'hobbies', payload: [...selectedHobby, data.item.id] })
        } else {
            const array = [...selectedHobby]
            const index = array.indexOf(data.item.id)
            array.splice(index, 1)
            setSelectedHobby(array)
            dispatch({ type: 'INPUT', id: 'hobbies', payload: array })
        }
        console.log('hobby : ', selectedHobby)
        console.log('hobby user : ', state.hobbies)
    }

    const showMultiDropdown = () => {
        console.log('alert dialog show')
        return (
            <Modal
                isVisible={isHobbyVisible}
                onBackButtonPress={() => setIsHobbyVisible(false)}
                onBackdropPress={() => setIsHobbyVisible(false)}
            >
                <View style={dialogStyle.container}>
                    <Text style={dialogStyle.dialogTitle}>Select Hobby</Text>
                    <FlatList
                        data={Hobbies}
                        renderItem={data => (
                            (
                                <TouchableOpacity
                                    style={[dialogStyle.itemContainer,
                                    state.hobbies.includes(data.item.id) ?
                                        { backgroundColor: Colors.greyDark } :
                                        { backgroundColor: Colors.grey }
                                    ]}
                                    onPress={() => {
                                        handleHobbyClicked(data)
                                    }}
                                >
                                    <View>
                                        <Text style={dialogStyle.item}>{data.item.hobby}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        )}
                        keyExtractor={item => item.id.toString()}
                        scrollEnabled={true}
                    />
                    <View style={{ flexDirection: 'row', display: 'flex', marginTop: 10 }}>
                        <View style={{ flex: 1, marginLeft: 10 }}>
                            <CustomButton types='primary' onPress={() => { setIsHobbyVisible(false) }} title='OK' />
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }

    const selectedDate = new Date(state.birthDate)

    const showDatePicker = () => {
        setIsDatePickerVisible(true)
    }

    const hideDatePicker = () => {
        setIsDatePickerVisible(false)
    }

    const handleDatePicker = (value) => {
        dispatch({ type: 'INPUT', id: 'isEdited', payload: true })
        hideDatePicker()
        dispatch({ type: 'INPUT', id: 'birthDate', payload: moment(value).format('YYYY/MM/DD') })
    }

    const handleSaveButton = () => {
        console.log(JSON.stringify(state, null, 2))
        dispatch({ type: 'INPUT', id: 'isLoading', payload: true })
        editProfile(state).then(response => {
            if (response['status'] == 1) {
                nav.goBack()
            } else {
                showErrorAlertDialog(
                    response['error']['errorMessage'],
                    false
                )
            }
            dispatch({ type: 'INPUT', id: 'isLoading', payload: false })
        })
            .catch(error => {
                showErrorAlertDialog(
                    'No Internet Connection',
                    false
                )
                dispatch({ type: 'INPUT', id: 'isLoading', payload: false })
            })
    }

    const handleOnChangeText = (value, name) => {
        dispatch({ type: 'INPUT', id: 'isEdited', payload: true })
        if (name == 'username') {
            if (!value.match(regex) && value != '') {
                return dispatch({ type: 'INPUT', id: 'username', payload: value, isValid: false, errorMsg: 'Only Accept Alphanumeric Character' })
            }
            if (value == '') {
                return dispatch({ type: 'INPUT', id: 'username', payload: value, isValid: false, errorMsg: 'This Field is Required' })
            }
            return dispatch({ type: 'INPUT', id: 'username', payload: value, isValid: true })
        } else {
            return dispatch({ type: 'INPUT', id: name, payload: value })
        }
    }

    return (
        <>
            <View style={styles.container}>
                <CustomToolbar
                    title='Edit Profile'
                    hasBack={true}
                    onPressBack={() => {
                        Keyboard.dismiss()
                        !state.isEdited ?
                            nav.goBack() :
                            showErrorAlertDialog(
                                'Data is not saved, do you want to discard the data?',
                                true,
                                () => {
                                    nav.goBack()
                                    closeAlert()
                                },
                                null,
                                'Yes'
                            )
                    }}
                    hasMenuBtn={true}
                    menuBtn={
                        <Text style={styles.btnMenuAction}>Save</Text>
                    }
                    disabled={!state.isValid}
                    onPressMenu={handleSaveButton}
                />
                <ScrollView>
                    <View style={styles.contentWrapper}>

                        <Text style={[editStyles.textLabel]}>Username</Text>
                        <TextInput
                            style={[styles.textInput]}
                            textContentType='username'
                            placeholder='Username'
                            value={state.username}
                            onChangeText={(value) => handleOnChangeText(value, 'username')}
                        />
                        {!state.isValid &&
                            <Text style={styles.errorMsg}>{state.errorUsername}</Text>
                        }
                        <Text style={[editStyles.textLabel]}>Date of Birth</Text>
                        <TouchableHighlight
                            underlayColor={Colors.greyLight}
                            onPress={showDatePicker}
                        >
                            <Text style={[styles.textInput, { height: 50 }, { textAlignVertical: 'center' }]}>
                                {
                                    state.birthDate != '' ? state.birthDate : 'Date of Birth'
                                }
                            </Text>
                        </TouchableHighlight>
                        <Text style={[editStyles.textLabel]}>Sex</Text>
                        <Dropdown
                            style={[styles.textInput]}
                            placeholderStyle={[editStyles.placeholderText]}
                            selectedTextStyle={editStyles.selectedText}
                            activeColor={Colors.greyLight}
                            data={Genders}
                            maxHeight={150}
                            labelField="gender"
                            valueField="id"
                            placeholder='Sex'
                            value={state.gender}
                            onChange={(value) => handleOnChangeText(value.id, 'gender')}
                        />
                        <Text style={[editStyles.textLabel]}>Occupation</Text>
                        <Dropdown
                            style={[styles.textInput]}
                            placeholderStyle={[editStyles.placeholderText]}
                            selectedTextStyle={editStyles.selectedText}
                            activeColor={Colors.greyLight}
                            data={Jobs}
                            maxHeight={150}
                            labelField='job'
                            valueField='id'
                            placeholder='Occupation'
                            value={state.job}
                            onChange={(value) => handleOnChangeText(value.id, 'job')}
                        />
                        <Text style={[editStyles.textLabel]}>Area</Text>
                        <Dropdown
                            style={[styles.textInput]}
                            placeholderStyle={[editStyles.placeholderText]}
                            selectedTextStyle={editStyles.selectedText}
                            activeColor={Colors.greyLight}
                            data={Residences}
                            maxHeight={150}
                            labelField='residence'
                            valueField='residence'
                            placeholder='Area'
                            value={state.residence}
                            onChange={(value) => handleOnChangeText(value.residence, 'residence')}
                        />
                        <Text style={[editStyles.textLabel]}>Hobby</Text>
                        <TouchableHighlight
                            underlayColor={Colors.greyLight}
                            onPress={() => setIsHobbyVisible(true)}
                        >
                            <Text style={[styles.textInput, { height: 50 }, { textAlignVertical: 'center' }]}>
                                {
                                    state.hobbies ?
                                        getHobbiesName() :
                                        'Hobby'
                                }
                            </Text>
                        </TouchableHighlight>
                        <Text style={[editStyles.textLabel]}>Character</Text>
                        <Dropdown
                            style={[styles.textInput]}
                            placeholderStyle={[editStyles.placeholderText]}
                            selectedTextStyle={editStyles.selectedText}
                            activeColor={Colors.greyLight}
                            data={Characters}
                            maxHeight={150}
                            labelField='character'
                            valueField='id'
                            placeholder='Character'
                            value={state.character}
                            onChange={(value) => handleOnChangeText(value.id, 'character')}
                        />
                        <Text style={[editStyles.textLabel]}>Freeword</Text>
                        <TextInput
                            style={[styles.textInput, { marginBottom: 4 }]}
                            placeholder='Freeword'
                            maxLength={200}
                            multiline={true}
                            numberOfLines={3}
                            value={state.freeword}
                            onChangeText={(text) => handleOnChangeText(text, 'freeword')}
                        />
                        <Text style={[{ textAlign: 'right' }, { color: 'grey' }]}>{state.freeword.length}/200</Text>
                    </View>
                </ScrollView>
                {
                    state.isLoading &&
                    <CustomLoader />
                }
                {showMultiDropdown()}
                <DateTimePicker
                    isVisible={isDatePickerVisible}
                    date={selectedDate}
                    mode='date'
                    onConfirm={(date) => {
                        handleDatePicker(date)
                    }}
                    onCancel={hideDatePicker}
                />
            </View>
        </>
    )
}

export default EditProfileScreen